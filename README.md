Steps to install:
1) git clone https://github.com/jatanpatel92/file_exec_node.git
2) cd /path/to/file_exec_node
3) npm install
4) Make changes to index.js in routes directory by insering the path to the exectable file.
5) Execute: sudo bin/www or for windows open cmd/git bash in 'Run as Administrator' mode
6) Open http://localhost:3000 in your browser.



     ,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--, 
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    ----------------------------------------------------------------- 


Happy coding!
